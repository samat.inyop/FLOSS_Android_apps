<!--
    Copyright (C)  2016 PRIMOKORN.
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3
    or any later version published by the Free Software Foundation;
    with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
    A copy of the license is included in the section entitled "GNU
    Free Documentation License".
-->
## LAUNCHER

* [App Launcher](https://f-droid.org/repository/browse/?fdid=com.simplemobiletools.applauncher): A simple holder for your favourite app launchers.
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/SimpleMobileTools/Simple-App-Launcher)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdid=com.simplemobiletools.applauncher)

***

* [Fast Access](http://v.ht/vR0D): A tiny launcher or as Samsung likes to call it Floating Toolbox.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/k0shk0sh/FastAccess)  
[![](Pictures/Google_Play.png)](https://play.google.com/store/apps/details?id=com.styleme.floating.toolbox.pro)  
_Uses Firebase (Firebase Cloud Messaging)_

***

* [G2L](http://v.ht/GZ0U): Define gestures to launch an action like an app, a phone call or playing a song.
![GPLv3+](https://img.shields.io/badge/License-GPLv3+-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-GitLab-lightgrey.svg?style=flat-square)](https://gitlab.com/easwareapps/g2l-gesture-launcher)  
[![](Pictures/F-Droid.png)](http://v.ht/GZ0U)

***

* [Hayai Launcher](https://f-droid.org/repository/browse/?fdid=com.hayaisoftware.launcher): A fast, lightweight search-based launcher.
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/seizonsenryaku/HayaiLauncher)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdid=com.hayaisoftware.launcher)

***

* [KISS launcher](http://v.ht/JqyL): Search through you app, contacts and settings lightning fast. No more time spent trying to find the app you need to launch: enter a few characters from the name and press enter. Need to phone someone? Don't meddle with the call log, just give three letters of their name and push the "phone" button.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/Neamar/KISS)  
[![](Pictures/F-Droid.png)](http://v.ht/JqyL)

***

* [LaunchTime](https://f-droid.org/repository/browse/?fdid=com.quaap.launchtime): Alternative homescreen which features a side menu used to organize your apps into common-sense and configurable categories.
![GPLv3+](https://img.shields.io/badge/License-GPLv3+-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/quaap/LaunchTime)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdid=com.quaap.launchtime)

***

* [OpenLauncher](https://play.google.com/store/apps/details?id=com.benny.openlauncher): This an open source Android launcher project, started from scratch. We aim to create a powerful and community driven Android launcher.
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/BennyKok/OpenLauncher)  
[![](Pictures/Google_Play.png)](https://play.google.com/store/apps/details?id=com.benny.openlauncher)

***

* [Search Based Launcher](https://f-droid.org/repository/browse/?fdid=com.vackosar.searchbasedlauncher): Minimalistic home-screen.
![MIT](https://img.shields.io/badge/License-MIT-orange.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/vackosar/search-based-launcher/)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdid=com.vackosar.searchbasedlauncher)

***

* [Silverfish](https://f-droid.org/repository/browse/?fdid=com.launcher.silverfish): A launcher with a "less is more" philosophy.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/stanipintjuk/Silverfish)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdid=com.launcher.silverfish)

***

* [SlimLauncher](http://v.ht/rWLo): lightweight aosp based launcher with lots of customizations.
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/SlimRoms/packages_apps_SlimLauncher)  
[![](Pictures/3rd-party.png)](http://forum.xda-developers.com/android/apps-games/app-slimlauncher-1-1-t3216971)

***

* [T-UI](http://v.ht/3DzZ): Launcher, which emulates a unix-like shell and its terminal interface, that is adpated for the use on mobile devices. It also implements a lot of commands.
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/Andre1299/TUI-ConsoleLauncher)  
[![](Pictures/F-Droid.png)](http://v.ht/3DzZ)

***

* [Trebuchet (CM13) for any ROM](http://v.ht/xQGp): Launcher, which emulates a unix-like shell and its terminal interface, that is adpated for the use on mobile devices. It also implements a lot of commands.
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/CyanogenMod/android_packages_apps_Trebuchet)  
[![](Pictures/3rd-party.png)](http://v.ht/xQGp)