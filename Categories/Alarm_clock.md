<!--
    Copyright (C)  2016 PRIMOKORN.
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3
    or any later version published by the Free Software Foundation;
    with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
    A copy of the license is included in the section entitled "GNU
    Free Documentation License".
-->
## ALARM CLOCK / TIMER / STOPWATCH

* [Clock+](https://f-droid.org/repository/browse/?fdfilter=clock&fdid=com.philliphsu.clock2): Alternative to the Google Clock app for Android, featuring UI/UX enhancements and new time pickers.
![GPLv3+](https://img.shields.io/badge/License-GPLv3+-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](http://v.ht/HXbd)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdfilter=clock&fdid=com.philliphsu.clock2)

***

* [OmniClock](http://v.ht/cybP): Based on AOSP DeskClock - developed as part of the OmniROM project.
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/maxwen/android_packages_apps_OmniClock)  
[![](Pictures/Google_Play.png)](https://play.google.com/store/apps/details?id=com.maxwen.deskclock)