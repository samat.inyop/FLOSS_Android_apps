<!--
    Copyright (C)  2016 PRIMOKORN.
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3
    or any later version published by the Free Software Foundation;
    with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
    A copy of the license is included in the section entitled "GNU
    Free Documentation License".
-->
## SOCIAL

* [AndStatus](https://f-droid.org/repository/browse/?fdid=org.andstatus.app): A low traffic social networking client with tree-like threaded conversations. It supports multiple protocols, including GNU social/StatusNet (e.g. Quitter, LoadAverage, Vinilox etc.), Twitter and Pump.io. It combines your accounts from all networks into one timeline and allows you reading and posting even while you are offline.
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/andstatus/andstatus)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdid=org.andstatus.app)

***

* [Dandelion*](http://v.ht/XfCe): dandelion* is a client for the community-run, distributed social network diaspora*. It adds some useful features to your networking experience.
![GPLv3+](https://img.shields.io/badge/License-GPLv3+-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/Diaspora-for-Android/dandelion)  
[![](Pictures/F-Droid.png)](http://v.ht/XfCe)

***

* [Face Slim](http://v.ht/VyZJ): Unofficial app built around the mobile Facebook site.
![GPLv2+](https://img.shields.io/badge/License-GPLv2+-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/indywidualny/FaceSlim)  
[![](Pictures/F-Droid.png)](http://v.ht/VyZJ)  
_Non-Free Network Services_

***

* [Kickstarter](https://play.google.com/store/apps/details?id=com.kickstarter.kickstarter): Kickstarter is a global community that helps bring creative projects to life. Explore thousands of projects in art, design, film, games, music, and more, and pledge to your favorites right from the app.
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/kickstarter/android-oss)  
[![](Pictures/Google_Play.png)](https://play.google.com/store/apps/details?id=com.kickstarter.kickstarter)

***

* [MaterialFBook](http://forum.xda-developers.com/android/apps-games/app-materialfbook-minimalist-facebook-t3477896): Minimalist facebook client.
![MIT](https://img.shields.io/badge/License-MIT-orange.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/ZeeRooo/MaterialFBook)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdfilter=MaterialFBook&fdid=me.zeeroooo.materialfb)  
_Non-Free Network Services_

***

* [Movim](https://f-droid.org/repository/browse/?fdfilter=movim&fdid=com.movim.movim): A distributed social networking platform that tries to protect your privacy and comes with a set of interesting features.
![AGPLv3+](https://img.shields.io/badge/License-AGPLv3+-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/movim/movim_android)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdfilter=movim&fdid=com.movim.movim)

***

* [RedReader](http://v.ht/q3wA): An unofficial client for the news site reddit.com.
![GPLv3+](https://img.shields.io/badge/License-GPLv3+-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/QuantumBadger/RedReader)  
[![](Pictures/F-Droid.png)](http://v.ht/q3wA)

***

* [Slide](http://v.ht/6fgH): Companion app for browsing Reddit.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/ccrama/Slide)  
[![](Pictures/F-Droid.png)](http://v.ht/6fgH)

***

* [SlimSocial for Facebook](http://v.ht/ZTvN) and [SlimSocial for Twitter](http://v.ht/C1Hy): Wrapper around Facebook's and Twitter's mobile website and APIs.
![GPLv2+](https://img.shields.io/badge/License-GPLv2+-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source Facebook-Github-lightgrey.svg?style=flat-square)](https://github.com/rignaneseleo/SlimSocial-for-Facebook) [![](https://img.shields.io/badge/Source Twitter-Github-lightgrey.svg?style=flat-square)](https://github.com/rignaneseleo/SlimSocial-for-Twitter)  
[![](Pictures/F-Droid.png)](http://v.ht/ZTvN) [![](Pictures/F-Droid.png)](http://v.ht/C1Hy)  
_Non-Free Network Services_

***

* [Surespot](https://play.google.com/store/apps/details?id=com.twofours.surespot): A mobile messaging application that secures all messages using end-to-end encryption.
![GPLv3+](https://img.shields.io/badge/License-GPLv3+-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/surespot/android)  
[![](Pictures/Google_Play.png)](https://play.google.com/store/apps/details?id=com.twofours.surespot)

***

* [Tusky](https://f-droid.org/repository/browse/?fdid=com.keylesspalace.tusky): Share words, photos, and videos with others.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/Vavassor/Tusky)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdid=com.keylesspalace.tusky)

***

* [Twidere](http://v.ht/hLKR): Microblogging client for Twitter.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/TwidereProject/Twidere-Android)  
[![](Pictures/F-Droid.png)](http://v.ht/hLKR)

***

* [WordPress](https://play.google.com/store/apps/details?id=org.wordpress.android): A client for WordPress blogs, which allows post creation and editing, and comment viewing, replying and moderation.
![GPLv2](https://img.shields.io/badge/License-GPLv2-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/wordpress-mobile/WordPress-Android)  
[![](Pictures/Google_Play.png)](https://play.google.com/store/apps/details?id=org.wordpress.android)  
_Non-free libraries like crashlytics. Latest F-Droid build dates back to 2014-04-12._