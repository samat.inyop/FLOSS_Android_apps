<!--
    Copyright (C)  2016 PRIMOKORN.
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3
    or any later version published by the Free Software Foundation;
    with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
    A copy of the license is included in the section entitled "GNU
    Free Documentation License".
-->
## GALLERY

* [A Photo Manager](http://v.ht/IhiE): Search photos in local media store for viewing and maintenance.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/k3b/androFotoFinder)  
[![](Pictures/F-Droid.png)](http://v.ht/IhiE)

***

* [ApertureGallery](https://github.com/MJonesDev/ApertureGallery).
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/MJonesDev/ApertureGallery)  
[![](Pictures/3rd-party.png)](https://github.com/MJonesDev/ApertureGallery/releases)

***

* [Gallery](http://v.ht/3C93): A gallery for viewing photos and videos.
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/SimpleMobileTools/Simple-Gallery)  
[![](Pictures/F-Droid.png)](http://v.ht/3C93)

***

* [GLImageViewer](http://v.ht/ntrF): Simple image viewer based on AOSP-Gallery app. It can open local/offline and remote/online images. It does not install an app launcher, but gets invoked from a browser or file explorer.
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/mariotaku/imageviewer-gl)  
[![](Pictures/F-Droid.png)](http://v.ht/ntrF)

***

* [Leafpic](http://v.ht/d2Wr): Advanced replacement for the default image gallery.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/HoraApps/LeafPic)  
[![](Pictures/F-Droid.png)](http://v.ht/d2Wr)

***

* [Opengur](http://v.ht/LaAl): Open source Imgur Android App.
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](http://v.ht/fD2Q)  
[![](Pictures/Google_Play.png)](http://v.ht/LaAl)  
_Includes non-free software: crashlytics/fabric_
