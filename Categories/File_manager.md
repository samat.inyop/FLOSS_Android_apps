<!--
    Copyright (C)  2016 PRIMOKORN.
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3
    or any later version published by the Free Software Foundation;
    with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
    A copy of the license is included in the section entitled "GNU
    Free Documentation License".
-->
## FILE MANAGER

* [Amaze](http://v.ht/Phjv): Light and smooth file manager following the Material Design guidelines.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/arpitkh96/AmazeFileManager)  
[![](Pictures/F-Droid.png)](http://v.ht/Phjv)

***

* [CyanogenMod for all](http://v.ht/dNpV): A file manager for AOSP, focused on rooted devices.
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/CyanogenMod/android_packages_apps_CMFileManager)  
[![](Pictures/3rd-party.png)](http://v.ht/dNpV)

***

* [File Manager](http://v.ht/5f8Vy): A simple file manager for browsing and editing files and directories.
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/SimpleMobileTools/Simple-File-Manager)  
[![](Pictures/F-Droid.png)](http://v.ht/5f8Vy)

***

* [Ghost commander](http://v.ht/VlNg): Dual panel file manager, like Norton Commander, Midnight Commander or Total Commander.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Sourceforge-lightgrey.svg?style=flat-square)](https://sourceforge.net/p/ghostcommander/code)  
[![](Pictures/F-Droid.png)](http://v.ht/VlNg)

***

* [Material File Manager](http://forum.xda-developers.com/android/apps-games/app-material-file-manager-source-t3492062): Basic Open Source File explorer developed following the Material Design guidelines.
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/CosimoSguanci/Material-File-Manager)  
[![](Pictures/Google_Play.png)](https://play.google.com/store/apps/details?id=app.android.com.materialfilemanager)