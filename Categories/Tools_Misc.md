<!--
    Copyright (C)  2016 PRIMOKORN.
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3
    or any later version published by the Free Software Foundation;
    with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
    A copy of the license is included in the section entitled "GNU
    Free Documentation License".
-->
## TOOLS / MISC

* [AROMA Installer](http://v.ht/34gX): For ROM Chefs/Developers only.
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/amarullz/AROMA-Installer)  
[![](Pictures/3rd-party.png)](http://v.ht/34gX)

***

* [Atomize](https://forum.xda-developers.com/android/apps-games/app-atomize-simple-png-image-compression-t3521092): Atomize can shrink the file size of an image significantly, often by as much as 70%, and maintains an image's transparency with little to no loss in quality. Atomize uses pngquant as its backend, and is fully opensource.
![GPLv2](https://img.shields.io/badge/License-GPLv2-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/wrmndfzzy/Atomize)  
[![](Pictures/Google_Play.png)](https://play.google.com/store/apps/details?id=com.wrmndfzzy.atomize)

***

* [BetterBatteryStats](http://v.ht/38wV): Advanced battery stats.
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/asksven/BetterBatteryStats)  
[![](Pictures/3rd-party.png)](http://forum.xda-developers.com/showpost.php?p=15869886&postcount=2)  
_Upstream Non-Free_

***

* [BARIA](http://v.ht/hBzu): Backup And Restore Installed Apps (apk only).
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-GitLab-lightgrey.svg?style=flat-square)](https://gitlab.com/easwareapps/BARIA/tree/HEAD)  
[![](Pictures/F-Droid.png)](http://v.ht/hBzu)

***

* [Busybox](http://v.ht/GTJt): Install a recent and un-crippled version of BusyBox.
![GPLv3+](https://img.shields.io/badge/License-GPLv3+-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/meefik/busybox)  
[![](Pictures/F-Droid.png)](http://v.ht/GTJt)

***

* [Dukto](https://play.google.com/store/apps/details?id=it.msec.dukto): An easy file transfer tool designed for LAN use. Transfer files from one PC (or other device) to another, without worrying about users, permissions, operating systems, protocols, clients, servers and so on...
![GPLv2](https://img.shields.io/badge/License-GPLv2-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Sourceforge-lightgrey.svg?style=flat-square)](https://sourceforge.net/projects/dukto/)  
[![](Pictures/Google_Play.png)](https://play.google.com/store/apps/details?id=it.msec.dukto&hl=fr)

***

* [FTP Server](http://v.ht/xwpM): This app can serve files over the internet but the most likely usage is to access files from another computer without needing a cable.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/ppareit/swiftp)  
[![](Pictures/F-Droid.png)](http://v.ht/xwpM)

***

* [Fingerprint2Sleep](http://v.ht/dAsL): Perform quick actions via tap fingerprint sensor.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/ztc1997/Fingerprint2Sleep)  
[![](Pictures/Google_Play.png)](https://play.google.com/store/apps/details?id=com.ztc1997.fingerprint2sleep)  
_Non-free: Google’s firebase_

***

* [GigaGet Download Manager](https://f-droid.org/repository/browse/?fdid=us.shandian.giga): Simple, multi-thread download manager.
![GPLv3+](https://img.shields.io/badge/License-GPLv3+-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/PaperAirplane-Dev-Team/GigaGet)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdid=us.shandian.giga)

***

* [Hangar](http://forum.xda-developers.com/showthread.php?t=2770533): Provides a customizable shortcut launcher in your notification drawer to give you relevant, easy access to your most frequently used apps based on smart filtering algorithms.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/corcoran/Hangar)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdfilter=Hangar&fdid=ca.mimic.apphangar) [![](Pictures/Google_Play.png)](https://play.google.com/store/apps/details?id=ca.mimic.apphangar)

***

* [LibreTasks](http://v.ht/2Ae2): Trigger actions when certain events happen. A fork of Omnidroid, now maintained and with more features. LibreTasks will allow you to set up certain triggers, such as plugging in your phone or missing a call, and allow you to specify certain actions that are done when that trigger occurs, such as sending a SMS or pausing your media.
![GPLv3,Apache2,LGPL](https://img.shields.io/badge/License-GPLv3,Apache2,LGPL-347D07.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/biotinker/LibreTasks/)  
[![](Pictures/F-Droid.png)](http://v.ht/2Ae2)

***

* [Mather](https://f-droid.org/repository/browse/?fdid=org.icasdri.mather): A powerful expression-based calculator, unit converter, and computation engine.
![GPLv3+](https://img.shields.io/badge/License-GPLv3+-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/icasdri/Mather)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdid=org.icasdri.mather)

***

* [MatLog](http://v.ht/OBE0): Material-style log reader for Android based on CatLog.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](Pictures/Google_Play.png)](http://v.ht/OBE0)  
_Uses non-free software, namely crashlytics - [Github issue](http://v.ht/FkWI)_

***

* [Night Screen](http://v.ht/aU7S): Prevent your eyes from being hurt by screen light.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/fython/Blackbulb)  
[![](Pictures/F-Droid.png)](http://v.ht/aU7S)

***

* [oandbackup](https://f-droid.org/repository/browse/?fdid=dk.jens.backup): Make backups of selected apps on your device and restore from those backups.
![MIT](https://img.shields.io/badge/License-MIT-orange.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/jensstein/oandbackup)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdid=dk.jens.backup)

***

* [Obsqr](http://v.ht/z9AI): Fast and simple QR code scanner that uses the zbar library. Minimalistic design allows you to access QR content with a single tap.
![MIT](https://img.shields.io/badge/License-MIT-orange.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/trikita/obsqr)  
[![](Pictures/F-Droid.png)](http://v.ht/z9AI)

***

* [ooniprobe](https://play.google.com/store/apps/details?id=org.openobservatory.ooniprobe): Open Observatory of Network Interference. A free software, global observation network for detecting censorship, surveillance and traffic manipulation on the internet.
![BSD](https://img.shields.io/badge/License-BSD-1EC9AA.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/TheTorProject/ooniprobe-android)  
[![](Pictures/Google_Play.png)](https://play.google.com/store/apps/details?id=org.openobservatory.ooniprobe)  
_Crashlytics_

***

* [OpenAPK](https://f-droid.org/repository/browse/?fdfilter=OpenAPK&fdid=com.dkanada.openapk): A material design app manager forked from ML Manager.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/dkanada/OpenAPK)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdfilter=OpenAPK&fdid=com.dkanada.openapk)

***

* [OpenGapps Downloader](https://f-droid.org/repository/browse/?fdfilter=opengapps&fdid=org.opengappsdownloader): App to auto download opengapps zip files from github.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/daktak/opengapps_downloader)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdfilter=opengapps&fdid=org.opengappsdownloader)  
_Non-Free add-ons_

***

* [Port Authority](https://f-droid.org/repository/browse/?fdid=com.aaronjwood.portauthority): A handy systems and security-focused tool, Port Authority is a very fast port scanner.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/aaronjwood/PortAuthority)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdid=com.aaronjwood.portauthority)

***

* [PowerToggles](http://v.ht/odu8): An advanced power control widget.
![MIT](https://img.shields.io/badge/License-MIT-orange.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/sunnygoyal/PowerToggles)  
[![](Pictures/Google_Play.png)](https://play.google.com/store/apps/details?id=com.painless.pc)

***

* [QR Scanner](http://v.ht/BVj7): Scan QR codes.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/SecUSo/privacy-friendly-qr-scanner)  
[![](Pictures/F-Droid.png)](http://v.ht/BVj7)

***

* [Rashr](http://v.ht/FIZR): Flash or backup your device's recovery and kernel without rebooting or PC or other hardware. Fast, Simple and Effective!.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/DsLNeXuS/Rashr/)  
[![](Pictures/Google_Play.png)](https://play.google.com/store/apps/details?id=de.mkrtchyan.recoverytools) [![](Pictures/3rd-party.png)](https://github.com/DsLNeXuS/Rashr/raw/master/RashrApp/RashrApp-release.apk)  
_Admob needs to be replaced with a dummy library_

***

* [Red Moon](http://v.ht/vKnu): Screen filter for night time phone use.
![GPLv3+](https://img.shields.io/badge/License-GPLv3+-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/raatmarien/red-moon)  
[![](Pictures/F-Droid.png)](http://v.ht/vKnu)

***

* [Screen Shift](https://f-droid.org/repository/browse/?fdfilter=Screen+Shift&fdid=com.sagar.screenshift2): Change screen resolution, density and overscan.
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/aravindsagar/ScreenShift)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdfilter=Screen+Shift&fdid=com.sagar.screenshift2)

***

* [Screens](https://f-droid.org/repository/browse/?fdid=uk.co.keepawayfromfire.screens): Automatically enter split screen mode from launcher shortcuts and Quick Settings tiles.
![SimplifiedBSD](https://img.shields.io/badge/License-SimplifiedBSD-1EC9AA.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/Cj-Malone/Screens)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdid=uk.co.keepawayfromfire.screens)

***

* [SELinuxToggler](https://forum.xda-developers.com/android/apps-games/app-selinuxtoggler-t3574688): This app can change the SELinux Modes without having to permanently modify the boot script files of the device.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/ibuprophen1/SELinuxToggler)  
[![](Pictures/3rd-party.png)](https://forum.xda-developers.com/devdb/project/?id=19837#downloads)

***

* [Share via HTTP](http://v.ht/eRI3d): Share files from your phone to one or many, quickly and easily.
![NewBSD](https://img.shields.io/badge/License-NewBSD-25B3D6.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/marcosdiez/shareviahttp)  
[![](Pictures/F-Droid.png)](http://v.ht/eRI3d)

***

* [SMS Backup+](http://v.ht/IL1T): Backup SMS and call logs to IMAP.
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/jberkel/sms-backup-plus)  
[![](Pictures/F-Droid.png)](http://v.ht/IL1T)

***

* [Stringlate](https://f-droid.org/repository/browse/?fdfilter=Stringlate&fdid=io.github.lonamiwebs.stringlate): Fetches remote GitHub repositories containing the source code for Android applications downloading the available `strings.xml` files in them for you to translate the strings to any other locale, in order to help the authors localize their applications into other languages.
![MIT](https://img.shields.io/badge/License-MIT-orange.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/LonamiWebs/Stringlate)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdfilter=Stringlate&fdid=io.github.lonamiwebs.stringlate)

***

* [Taskbar](http://v.ht/cAWj): Puts a start menu and recent apps tray on top of your screen accessible at any time.
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/farmerbb/Taskbar)  
[![](Pictures/F-Droid.png)](http://v.ht/cAWj)

***

* [Telecine](https://play.google.com/store/apps/details?id=com.jakewharton.telecine): Record full-resolution video on your Android devices.
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/JakeWharton/Telecine)  
[![](Pictures/Google_Play.png)](https://play.google.com/store/apps/details?id=com.jakewharton.telecine)  
_Non-Free software: Google Analytics_

***

* [Termux](https://f-droid.org/repository/browse/?fdid=com.termux): Termux combines powerful terminal emulation with an extensive Linux package collection.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/termux/termux-app)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdid=com.termux)

***

* [TouchDetector](https://f-droid.org/repository/browse/?fdfilter=TouchDetector&fdid=com.msapps.touchdetector): Display your finger touches on screen.
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/sujithkanna/TouchDetector)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdfilter=TouchDetector&fdid=com.msapps.touchdetector)

***

* [TrebleShot](http://v.ht/ESVR): An pair-to-pair file and text transferring application.
![](https://img.shields.io/badge/License-Missing-000000.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/genonbeta/TrebleShot)  
[![](Pictures/3rd-party.png)](https://github.com/genonbeta/TrebleShot/releases/)

***

* [Wikipedia](http://v.ht/3X1S): Official Wikipedia application.
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/wikimedia/apps-android-wikipedia)  
[![](Pictures/F-Droid.png)](http://v.ht/3X1S)  
_Tracks You_

***

* [waut.ch!](https://forum.xda-developers.com/android/apps-games/app-waut-ch-calibration-android-t3549967): Utility for background calibration, curation and tuning of the device towards an intuitive interface.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/Openand-I)  
[![](Pictures/Google_Play.png)](https://play.google.com/store/apps/details?id=ch.waut)

***

* [Xposed Installer](https://f-droid.org/app/de.robv.android.xposed.installer): Official installer for the Xposed framework.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/rovo89/XposedInstaller)  
[![](Pictures/F-Droid.png)](https://f-droid.org/app/de.robv.android.xposed.installer)