<!--
    Copyright (C)  2016 PRIMOKORN.
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3
    or any later version published by the Free Software Foundation;
    with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
    A copy of the license is included in the section entitled "GNU
    Free Documentation License".
-->
## SECURITY / PRIVACY

* [AdAway](http://v.ht/Y8hn): Manage your hosts file to block ads, malwares, tracking....
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/Free-Software-for-Android/AdAway/)  
[![](Pictures/3rd-party.png)](http://v.ht/Y8hn)

***

* [AFWall+](http://v.ht/U5yM): An improved version of DroidWall (front-end application for the powerful iptables Linux firewall). It allows you to restrict which applications are permitted to access your data networks (2G/3G/4G/LTE and/or Wi-Fi and while in roaming).
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/ukanth/afwall)  
[![](Pictures/F-Droid.png)](http://f-droid.org/repository/browse/?fdfilter=afwall+&fdid=dev.ukanth.ufirewall) [![](Pictures/Google_Play.png)](https://play.google.com/store/apps/details?id=dev.ukanth.ufirewall)  
_Non-Free Addons_

***

* [Bitwarden](http://v.ht/5uWB): Stores all of your logins and passwords while conveniently keeping them synced between all of your devices (multi platforms). 100% free.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/bitwarden)  
[![](Pictures/Google_Play.png)](https://play.google.com/store/apps/details?id=com.x8bit.bitwarden) [![](Pictures/3rd-party.png)](https://github.com/bitwarden/mobile/releases/)

***

* [DNS66](http://v.ht/Uy3w): Allows blocking host names via DNS. This can be used for ad blocking. It also allows other DNS servers to be added, for more privacy.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/julian-klode/dns66)  
[![](Pictures/F-Droid.png)](http://v.ht/Uy3w)

***

* [FreeOTP](http://v.ht/mVje): FreeOTP is a two-factor authentication (2FA) application for systems utilizing one-time password protocols (OTP).
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://fedorahosted.org/freeotp/browser/android)  
[![](Pictures/F-Droid.png)](http://v.ht/mVje)

***

* [FreeOTP+](http://v.ht/1obJ): Fork of FreeOTP. Google Authenticator replacement with import/export support.
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/helloworld1/FreeOTPPlus)  
[![](Pictures/Google_Play.png)](https://play.google.com/store/apps/details?id=org.liberty.android.freeotpplus)

***

* [Keepass2Android](http://v.ht/PPE4): Fork of KeePassDroid. Password manager. It reads and writes .kdbx-files, the database format used by the popular KeePass 2.x Password Safe.
![GPLv2](https://img.shields.io/badge/License-GPLv2-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](http://v.ht/S3EI)  
[![](Pictures/Google_Play.png)](http://v.ht/PPE4)

***

* [KeePassDroid](http://v.ht/rrOC): A password safe, compatible with KeePass.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/bpellin/keepassdroid)  
[![](Pictures/F-Droid.png)](http://v.ht/rrOC)

***

* [Lesspass](https://lesspass.com/): LessPass derives a site, a login and a master password to generate a unique password. This means you can manage your passwords locally without having to sync your password vault across every device.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/lesspass/cordova)  
[![](Pictures/Google_Play.png)](https://play.google.com/store/apps/details?id=com.lesspass.android)

***

* [Master Password](http://www.devland.de/): Password manager that doesn’t store your personal passwords on the device. Instead it generates all passwords on the demand by using a single master password that never leaves the device.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/dkunzler/masterpassword)  
[![](Pictures/Google_Play.png)](https://play.google.com/store/apps/details?id=de.devland.masterpassword)

***

* [NetGuard](http://forum.xda-developers.com/android/apps-games/app-netguard-root-firewall-t3233012): Simple way to block access to the internet - no root required. Applications can individually be allowed or denied access via your Wi-Fi and/or mobile connection.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/M66B/NetGuard)  
[![](Pictures/3rd-party.png)](https://github.com/M66B/NetGuard/releases/)  
_Last [F-Droid](http://v.ht/H82W) version is 2.44 since later versions have non-free dependencies (Firebase)_

***

* [OpenVPN for Android](http://v.ht/CjYn): OpenVPN without root.
![GPLv2](https://img.shields.io/badge/License-GPLv2-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/schwabe/ics-openvpn)  
[![](Pictures/F-Droid.png)](http://v.ht/CjYn)

***

* [OTP Authenticator](http://v.ht/GFFu): A two-factor authentication App for Android 4.0+.
![MIT](https://img.shields.io/badge/License-MIT-orange.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/0xbb/otp-authenticator)  
[![](Pictures/F-Droid.png)](http://v.ht/GFFu)

***

* [Password Store](https://f-droid.org/repository/browse/?fdfilter=password+store&fdid=com.zeapo.pwdstore): Simple password manager that is compatible with [pass](http://www.passwordstore.org/): Passwords are stored in simple text files which are encrypted with OpenPGP.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/zeapo/Android-Password-Store)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdfilter=password+store&fdid=com.zeapo.pwdstore)

***

* [PixelKnot](http://v.ht/AFqD): Hide messages inside files.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/guardianproject/PixelKnot)  
[![](Pictures/F-Droid.png)](http://v.ht/AFqD)

***

* [PornAway](http://forum.xda-developers.com/android/apps-games/root-pornaway-block-porn-sites-t3460036): Extracted version of AdAway to block porn sites.
![](https://img.shields.io/badge/License-Missing-000000.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/mhxion/pornaway/)  
[![](Pictures/3rd-party.png)](https://github.com/mhxion/pornaway)

***

* [Prey](https://play.google.com/store/apps/details?id=com.prey): Prey is a lightweight theft protection software that lets you keep an eye over them whether in town or abroad, and helps you recover them if ever lost or stolen.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/prey/prey-android-client)  
[![](Pictures/Google_Play.png)](https://play.google.com/store/apps/details?id=com.prey)  
_Non-free dependencies: Play Services_

***

* [Wi-Fi Privacy Police](http://v.ht/xHWR): Prevents your smartphone or tablet from leaking privacy sensitive information via Wi-Fi networks.
![GPLv2+](https://img.shields.io/badge/License-GPLv2+-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/BramBonne/privacypolice)  
[![](Pictures/F-Droid.png)](http://v.ht/xHWR)

***

* [XPrivacy](http://v.ht/Iw2y): XPrivacy can prevent applications from leaking privacy sensitive data. XPrivacy can restrict the categories of data an application can access.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/M66B/XPrivacy)  
[![](Pictures/3rd-party.png)](https://github.com/M66B/XPrivacy/releases/)  
_3rd party apps not allowed - [Github ref](https://github.com/M66B/XPrivacy#FAQ72)_