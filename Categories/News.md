<!--
    Copyright (C)  2016 PRIMOKORN.
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3
    or any later version published by the Free Software Foundation;
    with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
    A copy of the license is included in the section entitled "GNU
    Free Documentation License".
-->
## NEWS / RSS FEEDS

* [Feeder](https://f-droid.org/repository/browse/?fdfilter=Feeder&fdid=com.nononsenseapps.feeder): This is a no-nonsense RSS/Atom feed reader app for Android.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/spacecowboy/feeder)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdfilter=Feeder&fdid=com.nononsenseapps.feeder)

***

* [Flym](http://v.ht/4HY6) and [spaRSS](http://v.ht/626Z): It checks RSS/Atom news feeds, polling for updates from the device on a regular basis. Fetched items are available for offline reading.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source Flym-Github-lightgrey.svg?style=flat-square)](https://github.com/FredJul/Flym) [![](https://img.shields.io/badge/Source spaRSS-Github-lightgrey.svg?style=flat-square)](https://github.com/Etuldan/spaRSS)  
[![](Pictures/F-Droid.png)](http://v.ht/4HY6) [![](Pictures/F-Droid.png)](http://v.ht/626Z)

***

* [HN](http://v.ht/3esC): Hacker News client with a focus on reliability and usability.
![MIT](https://img.shields.io/badge/License-MIT-orange.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/manmal/hn-android)  
[![](Pictures/F-Droid.png)](http://v.ht/3esC)  
_The upstream source code is non-free._

***

* [Materialistic](http://v.ht/o2Ld): Hacker News client, designed for both phones & tablets, with optimized speed & network.
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/hidroh/materialistic)  
[![](Pictures/F-Droid.png)](http://v.ht/o2Ld)  
_Non-Free network service_