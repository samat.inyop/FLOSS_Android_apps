# List of Free / Libre and Open Source apps for Android

![FLOSS](https://img.xda-cdn.com/G5UvJb4_j1OTylVCZviLoLZEyrA=/http%3A%2F%2Fimagik.fr%2Fimages%2F2016%2F10%2F29%2FFLOSS_XDAabad3.png)

GitLab repo of my [XDA thread](http://forum.xda-developers.com/android/general/index-floss-list-free-libre-source-apps-t3482219/).

![Number of apps](https://img.shields.io/badge/Apps-265-brightgreen.svg?style=flat-square)  

![License](Pictures/gnu-fdl.png)  

![Twitter](https://img.shields.io/badge/Twitter-@primokorn-blue.svg?style=flat-square)

---

I use Android phones for 5 years but I'm interested in FLOSS development for only 1 year. Conlusion? `FLOSS apps` are very important for the community (not only for Android OS) but they are often underestimated or not well known.
Many awesome FLOSS apps are available for `Android` and they well deserve to be better known! Yes, there's a world behind Google, Amazon, Facebook, Apple, Microsoft,...

> If you want to discover new free/libre and open source apps, give a try to the below apps! I'll add new apps as soon as I try good ones.

> If you want to suggest new apps, feel free to share your list! :innocent:

> If you want to keep using proprietary apps, go away!!! :japanese_goblin:


***

## INDEX

| **CATEGORIES** | **SHORT DESCRIPTION** | 
| :----------- | :----------- | 
[ALARM CLOCK](Categories/Alarm_clock.md) | Alarm clock, timer and stopwatch
[BOOKS & REFERENCES](Categories/Books.md) | Translation, books...
[BUSINESS](Categories/Business.md) | Helps you to work more efficiently
[CALENDAR](Categories/Calendar.md) | Manage your calendars and contacts
[COMMUNICATION](Categories/Communication.md) | IM / SMS / MMS / Chat / Team collaboration
[ENTERTAINMENT](Categories/Entertainment.md) | Movies, TV...
[EMAIL](Categories/Email.md) | Self-explanatory
[FINANCE](Categories/Finance.md) | New currencies like Bitcoin
[FOOD & DRINK](Categories/Food_drink.md) | Know what you eat and drink
[FRAMEWORK / SERVICES](Categories/Framework.md) | Alternative to Google Play Services, push notifications...
[FILE MANAGER](Categories/File_manager.md) | Self-explanatory
[GALLERY](Categories/Gallery.md) | View your pictures and videos
[HEALTH and FITNESS](Categories/Health_Fitness.md) | To be healthy and stay healthy
[KEYBOARD](Categories/Keyboard.md) | Self-explanatory
[LAUNCHER](Categories/Launcher.md) | Home and App launchers
[MONITOR](Categories/Monitor.md) | Tools to monitor or manage Android
[MUSIC](Categories/Music.md) | Self-explanatory
[NAVIGATION](Categories/Navigation.md) | Maps, GPS, Transport
[NEWS](Categories/News.md) | RSS Feeds specific news
[PHOTOGRAPHY](Categories/Photography.md) | Take photos and videos
[PRODUCTIVITY](Categories/Productivity.md) | Apps that save time
[NOTES](Categories/Notes.md) | Reminders, to-do lists and notes
[ROOT](Categories/Root.md) | Specific app to obtain and manage root
[SECURITY](Categories/Security.md) | Improve your security and privacy
[SELF-HOSTING](Categories/Self_hosting.md) | Host files yourself or use free/libre services
[SHOPPING](Categories/Shopping.md) | Self-explanatory
[SOCIAL](Categories/Social.md) | Spend time on Diaspora*, Twitter, Reddit, WordPress...
[THEMING](Categories/Theming.md) | Custom theme, icon pack, wallpaper
[TOOLS & MISC](Categories/Tools_Misc.md) | Many tools and stuff with no dedicated category
[TORRENT](Categories/Torrent.md) | Self-explanatory
[VIDEO / AUDIO PLAYERS](Categories/Video_Audio_Players.md) | Video/Audio players and editors
[VEHICLES](Categories/Vehicles.md) | Auto, taxi, gas
[WEATHER](Categories/Weather.md) | Self-explanatory
[WEB BROWSER](Categories/Web.md) | Self-explanatory

***


## REFERENCES
- [What is free software?](http://v.ht/BCtj)
- [Why 'Free Software' is better than 'Open Source'](http://v.ht/BmlF)
- [Various Licenses and Comments about Them](http://v.ht/lChF)
- [F-Droid's Inclusion Policy](http://v.ht/iTAZo)
- [F-Droid's Antifeatures](https://f-droid.org/wiki/page/Antifeatures)
- [alternativeto.net](http://v.ht/0vYQ)
- [Droid-Break](http://v.ht/8K8z)
- [FLOSS and FOSS](http://v.ht/uw6c)
- [Why Open Source misses the point of Free Software](http://v.ht/G4pm)
- [Liberate Your Device!](http://v.ht/aHEk)
- [Android without Google](https://android.izzysoft.de/articles/named/android-without-google-1)  
- [How to use Android without Google Play Services](https://shadow53.com/no-gapps/)

***


## HOW TO CONTRIBUTE
1. Only **one** app per commit
2. **Search** in the existing list and the closed issues before creating a Pull Request or a new Issue.
3. Provide links, especially the **source code**.

***


## DIFFERENCES COMPARED TO F-DROID
Several apps listed here are not available on F-Droid. F-Droid is a `FLOSS app store` so they decline any open source apps that don't respect their **Inclusion Policy**. By the way, some FLOSS apps aren't available on F-Droid (lack of time or big apps to maintain).
I also tested more than 80% of the apps I posted in this thread so this is more or less my recommended app list.